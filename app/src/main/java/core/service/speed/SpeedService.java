package core.service.speed;

import core.data.speed.SpeedData;
import core.data.speed.SpeedEnabledData;

public abstract class SpeedService
{
    public static String test(String val) {
        if(SpeedEnabledData.getEnabled()) {
            SpeedData speedData = new SpeedData(Float.parseFloat(val));
            return String.valueOf(speedData.increase(1));
        }
        else
        {
            return val;
        }
    }
}
