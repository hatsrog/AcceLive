package core.data.geolocation;

public abstract class GeolocationEnabledData
{
    private static boolean isEnabled;

    public static boolean getEnabled()
    {
        return isEnabled;
    }

    public static void setEnabled(boolean enabledValue)
    {
        isEnabled = enabledValue;
    }
}
