package core.data.speed;

import core.data.distance.DistanceInf;
import core.service.speed.SpeedService;

public class SpeedData {
    // MEMBERS
    private float actualSpeed;

    public SpeedData() {
    }

    public SpeedData(float initialValue) {
        this.actualSpeed = initialValue;
    }

    public float increase(float val) {
        return actualSpeed += val;
    }
}
