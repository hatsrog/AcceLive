package core.data.speed;

public abstract class SpeedEnabledData {

    private static boolean isEnabled = true;

    public static void setEnabled(boolean value)
    {
        isEnabled = value;
    }

    public static boolean getEnabled()
    {
        return isEnabled;
    }
}
