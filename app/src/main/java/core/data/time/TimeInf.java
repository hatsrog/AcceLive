package core.data.time;

public interface TimeInf {
    int SECOND = 1;
    int MINUTE = 60;
    int HOUR = 3600;
}
