package core.data.time;

public abstract class TimeEnabledData {

    private static boolean isEnabled = true;

    private static void setEnabled(boolean value)
    {
        isEnabled = value;
    }

    private static boolean getEnabled()
    {
        return isEnabled;
    }
}
