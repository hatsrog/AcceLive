package core.data.distance;

public interface DistanceInf {
    int KILOMETER = 1000;
    int METER = 1;
    float MILE = 1609.34f;
    float YARD = 0.9144f;
}
