package core.data.distance;

public class DistanceData {
    private float distanceUnit;
    private float distance;

    public DistanceData(float distUnit, float init)
    {
        this.distanceUnit = distUnit;
        this.distance = init;
    }

    public void convert(float unit)
    {
        float ratio = this.distanceUnit / unit;
        this.distance *= ratio;
        this.distanceUnit = unit;
    }

    public float getDistance()
    {
        return this.distance;
    }

    public float getUnit()
    {
        return this.distanceUnit;
    }
}
