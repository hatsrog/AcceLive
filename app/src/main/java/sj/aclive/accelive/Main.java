package sj.aclive.accelive;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import core.data.distance.DistanceData;
import core.data.distance.DistanceInf;
import core.data.geolocation.GeolocationEnabledData;
import core.data.speed.SpeedEnabledData;
import core.service.speed.SpeedService;

public class Main extends AppCompatActivity {

    private static final int LOCATION_REFRESH_TIME = 100;
    private static final float LOCATION_REFRESH_DISTANCE = 0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView latitude = (TextView)findViewById(R.id.textViewLatitude);
        final TextView longitude = (TextView)findViewById(R.id.textViewLongitude);

        final LocationListener mLocationListener = new LocationListener() {


            @Override
            public void onLocationChanged(final Location location) {
                latitude.setText(String.valueOf(location.getLatitude()));
                longitude.setText(String.valueOf(location.getLongitude()));
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        final LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        final Button button = (Button)findViewById(R.id.button2);
        final Switch switchEnabled = (Switch)findViewById(R.id.switch1);
        final Button buttonChangeActivity = (Button)findViewById(R.id.buttonChangeActivity);
        final Button buttonONOFF = (Button)findViewById(R.id.buttonONOFF);
        SpeedEnabledData.setEnabled(switchEnabled.isChecked());
        switchEnabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked)
            {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                else {
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME,
                            LOCATION_REFRESH_DISTANCE, mLocationListener);
                }
            }
            else
            {
                mLocationManager.removeUpdates(mLocationListener);
            }
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                button.setText(SpeedService.test(button.getText().toString()));
            }
        });
        buttonChangeActivity.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            Intent intent = new Intent(Main.this, About.class);
            startActivity(intent);
            }
        });
        if(GeolocationEnabledData.getEnabled())
        {
            buttonONOFF.setText("ON");
        }
        else
        {
            buttonONOFF.setText("OFF");
        }

        buttonONOFF.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view)
            {
                if(GeolocationEnabledData.getEnabled())
                {
                    buttonONOFF.setText("OFF");
                    GeolocationEnabledData.setEnabled(false);
                }
                else
                {
                    buttonONOFF.setText("ON");
                    GeolocationEnabledData.setEnabled(true);
                }
            }
        });
    }
}
